_Script creado en el curso "Using Python for Research"_

Despues de la transcripción (No se procesa en el script),
se produce el segundo proceso llamado traducción del ADN en el que se crean proteínas

- En primer lugar, hemos descargado manualmente los datos de las secuencias de ADN y proteínas
- En segundo lugar, hemos importado los datos de ADN a Python.
- A continuación, hemos creado un algoritmo que ha traducido el ADN utilizando un diccionario.
- Por último, comprobamos que la traducción del ADN
coincidía con la secuencia de aminoácidos descargada


> los datos se encuentran en el mismo directorio del script
