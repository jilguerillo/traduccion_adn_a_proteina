#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 02:07:25 2021

@author: jilguerillo

Despues de la transcripción (No se procesa en el script),
se produce el segundo proceso llamado traducción del ADN en el que se crean proteínas

- En primer lugar, hemos descargado manualmente los datos de las secuencias de ADN y proteínas
- En segundo lugar, hemos importado los datos de ADN a Python.
- A continuación, hemos creado un algoritmo que ha traducido el ADN utilizando un diccionario.
- Por último, comprobamos que la traducción del ADN
coincidía con la secuencia de aminoácidos descargada

"""

def read_seq(inputfile):
   """Lee y devuelve la secuencia de entrada con los caracteres especiales eliminados"""    
   with open(inputfile, "r") as f:
       seq = f.read()
   seq = seq.replace("\n", "")
   seq = seq.replace("\r", "")
   return seq


def translate(seq):
    """
    Traduce una cadena que contiene una secuencia de nucleótidos (adenina, 
    timina (T) -> uracilo (U), guanina y citosina (A/U,G/C) en una 
    cadena que contiene la correspondiente secuencia de aminoácidos. 
    Los nucleótidos se traducen en tripletas utilizando el diccionario de 
    la tabla; cada aminoácido 4 se codifica con una cadena de longitud 1.

    Tabla de ARN de transferencia = ARNm + su correspondiente aminoácido 
    """

    table = {
        'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
        'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
        'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
        'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
        'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
        'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
        'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
        'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
        'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
        'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
        'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
        'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
        'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
        'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
        'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
        'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }
    
    protein = ""
    # estructura tridimensional
    if len(seq) % 3 == 0:
        for i in range(0, len(seq), 3):
            # cadena de tres nucleótidos del ARNm (A, G, C y U) es un codon
            # se corresponde con un aminoácido en específico, o con una señal 
            # de inicio o terminación
            codon = seq[i : i+3]
            protein += table[codon]
            
    return protein

# comprobamos que la traducción del ADN
prt == translate(dna[20:938])[:-1]
translate(dna[20:938])[:-1] == translate(dna[20:935])
